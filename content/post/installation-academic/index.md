---
title: Hugo Academic
date: 2020-03-10
math: false
diagram: false
draft: false
---

[Academic](https://sourcethemes.com/academic/) ist ein [Hugo](https://gohugo.io)-basiertes Framework, mit dem schnell Seiten mit technischem Inhalt erstellt werden können. Unter Fedora 31 erfolgt die Installation wie folgt.

# Installation

## Hugo

Academic benötigt Hugo in der Extended-Variante, welche nicht in den Repos von Fedora enthalten ist. Deshalb nutze ich Snapd.

```sh
$ sudo dnf install snapd
$ sudo snap install hugo --channel=extended/stable
```

## Academic Framework

Das Academic Framework wird geforkt und testweise der lokale Server gestartet.

```sh
$ git clone https://github.com/sourcethemes/academic-kickstart.git Meine_Webseite
$ cd Meine_Webseite
$ git submodule update --init --recursive
$ hugo server
```

> Um unnötigen Müll zu vermeiden, empfiehlt sich ein anderes Vorgehen (ungetestet): https://pirivan.gitlab.io/post/2019-05-05/.
> 
> In meinem Fall nehme ich die in dem Link beschriebenen Änderung nachträglich vor (aber idealerweise vor dem ersten Commit ins eigene Repository...):
> ```sh
> $ rm -Rf .git
> $ rm -Rf themes/academic
> $ git remote add hugo-academic https://github.com/gcushen/hugo-academic.git
> $ git subtree add --prefix=themes/academic hugo-academic master --squash
> $ git branch -a
>  * master
>    remotes/hugo-academic/master
> ```
> Ein Update des Themes kann dann jederzeit vorgenommen werden.
> ```sh
> $ git subtree pull --prefix=themes/academic hugo-academic master --squash 
> ```

Üblicherweise kann nun der Webserver über http://localhost:1313 erreicht werden.

Beispielinhalte sind unter themes/academic/exampleSite und können einfach ins root-Verzeichnis kopiert werden.

```sh
$ cp -av themes/academic/exampleSite/* .
```

## eMail Obfuscation

Auch wenn es Kritiker der Methode gibt. Neben einem guten Filter kann es nicht Schaden einen Obfuscator wie [Hugo-Cloark-eMail](https://github.com/martignoni/hugo-cloak-email) zu verwenden.

```sh
$ git remote add hugo-cloak-email https://github.com/martignoni/hugo-cloak-email.git
$ git subtree add --prefix=themes/hugo-cloak-email hugo-cloak-email master --squash
$ git branch -a
```

Hugo-Cloak-eMail muss vor dem eigentlichem Theme geladen werden (hier über config/_default/config.toml konfiguriert).

```toml
...
theme = ["hugo-cloak-email", "academic"]
...
```

Dann können verschiedene Angaben bspw. im Impressum kodiert werden.

```md
Telefon: {{< cloakemail address="+49 (0) 123 44 55 66" protocol="tel">}}\
Telefax: {{< cloakemail address="+49 (0) 123 44 55 99" protocol="tel">}}\
E-Mail: {{< cloakemail "mustermann@musterfirma.de" >}}
```

> Aufgrund fehlendes Bedarfs hat sich praktisch kein Fax-Protokoll durchgesetzt und die entsprechende Regelung obsolet (https://stackoverflow.com/a/9443530). Deshalb wird auch hier tel als Protokoll eingesetzt.