---
title: Rechtschreibung
date: 2020-03-13
math: false
diagram: false
draft: false
---

Schrecklich wenn einem auffällt, welche Fehler man seit Jahrzehnten macht... Da das Weblog aber nur für mich ist, muss es auch nur mich ärgern.

> Wer sich hierher verirrt hat: Ich übernehme weder Haftung für das was man aus den Inhalten meines Weblogs macht oder für die seelischen Schäden, die man aufgrund der Grammatik und Rechtschreibung davonträgt...

Falsch          | Korrekt        | Bemerkung
--------------- | -------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------
Daalja's Weblog | Daaljas Weblog | Apostroph nur bei der Genetivbildung von Namen, die auf einen s-Laut enden oder ohne Apostroph verwechselt werden können (Andreas Laden oder Andrea's Laden)

# Weitere Informationen

https://www.studis-online.de/Studieren/Richtig_schreiben/apostroph.php