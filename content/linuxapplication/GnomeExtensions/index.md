---
title: Gnome Extensions
summary: Gnome Extensions
tags:
- hosting
- blog
date: "2020-04-04T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: ""

url_code: "https://extensions.gnome.org"

---

Für Gnome 3 haben sich bei mir die Extensions bewährt:

# Extensions.gnome.org
* (Applications Menu)[https://extensions.gnome.org/extension/6/applications-menu/]
* (Caffeine)[https://extensions.gnome.org/extension/517/caffeine/]
* (CPU Power Manager)[https://extensions.gnome.org/extension/945/cpu-power-manager/]
* (Dash to Panel)[https://extensions.gnome.org/extension/1160/dash-to-panel/]
* (Hibernate Status Button)[https://extensions.gnome.org/extension/755/hibernate-status-button/]
# Manual
* (Desaturate All)[https://github.com/laerne/desaturate_all]
* 