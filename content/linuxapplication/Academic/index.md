---
title: Academic Hugo-Framework
summary: Markdown-Framework für technische Blogs
tags:
- hosting
- blog
date: "2020-03-11T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: ""

url_code: "https://sourcethemes.com/academic/"

---

[Academic](https://sourcethemes.com/academic/) ist ein Markdown-Framework das mittels [Hugo](https://gohugo.io) einfach und schnell professionelle technische Blogs generiert.