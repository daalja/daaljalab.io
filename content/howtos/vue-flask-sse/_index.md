---
# Course title, summary, and position.
linktitle: Vue-Flask-SSE
summary: Kombiniere ein Vue-Frontend mit einem Flask-Backend (Python) mit SSE
weight: 3

# Page metadata.
title: Vue-Flask-SSE
date: "2020-03-14T00:00:00Z"
lastmod: "2020-03-14T00:00:00Z"
draft: true  # Is this a draft? true/false
toc: true  # Show table of contents? true/false
type: docs  # Do not modify.

menu:
  vue-flask-sse:
    name: Übersicht
    weight: 3
---

## Server-sent events (SSE)

Siehe [MDN webdocs](https://developer.mozilla.org/en-US/docs/Web/API/Server-sent_events)

## Flask Server

```python
from flask import Flask, Response
from flask_cors import CORS
from threading import Thread, Lock
import time
import queue
import json

thread = None
messages = queue.Queue(21)

def background_update(messages):
    if messages.empty():
        # Vermutlich brauchen wir ein Lock, da ansonsten parallel auf die Queue zugegriffen wird?
        print("New list")
        message = {"total_items": 20, "msg": "Hellos from the server"}
        messages.put(json.dumps(message))
        for i in range(1, 21):
            print(i)
            message = {"cnt": i, "text": "Hello_{}".format(i)}
            messages.put(json.dumps(message))
    else:
        print("Queue not empty.")
    time.sleep(2)

app = Flask(__name__)
CORS(app)

@app.route("/")
def start():
    return "Hello World"

@app.route("/stream")
def stream():
    def eventStream():
        global messages
        global thread
        if thread is None:
            thread = Thread(target=background_update, args=(messages, ))
            thread.start()
            print("Backgroundthread started...")
        while True:
            if not messages.empty():
                # Der Browser erwartet eine Nachricht der Art: "data: <...>\n\n"
                message = messages.get()
                eventtype = 'header' if 'total_items' in message else 'item'
                print("Sending: {}".format(message))
            else:
                print("Close Stream.")
                eventtype = 'close'
                message = ''
                thread = None
            time.sleep(0.3)
            yield "event: {}\ndata: {}\n\n".format(eventtype, message)
    return Response(eventStream(), mimetype="text/event-stream")

if __name__ == "__main__":
    app.run(debug=True)
```

## Vue

Die Hello World Komponent die durch `vue create frontend` generiert wurde, wird nun angepasst:

```js
<template>
  <div class="container">
    <form v-on:submit.prevent="setupStream">
        <input v-model="msg" type="text" placeholder="Enter message"/>
        <button v-on:click="setupStream" type="button">{{ buttonLabel }}</button>
      </form>
      <p>{{ items.length }} of {{ total_items }} times “{{ act_msg }}”:</p>
      <ul v-for="item in items" :key="item.cnt" v-bind:item="item">
        <li>{{ item.text }}</li>
      </ul>
  </div>
</template>

<script>
export default {
  name: 'HelloWorld',
  data () {
    return {
      msg: 'Hello World',
      act_msg: '',
      total_items: -1,
      items: [],
      loading: false
    }
  },
  computed: {
    buttonLabel() {
      return (this.loading ? 'Loading...' : 'Go');
    }
  },
  created() {
    this.setupStream();
  },
  methods: {
    setupStream() {
      /* 1. Incomming header message => item count known
       * 2. Incoming items
       * 2.1. Client will close the connection when it received all items
       * 2.2. Server can close the connection at any time. It will
       *      even try to close the connection after sending all items
       *      (as long as the connection is still established)
      */
      console.log("Starting SSE...")
      let es = new EventSource('http://127.0.0.1:5000/stream');
      es.addEventListener('header', event => {
        this.items.length = 0;
        let header = JSON.parse(event.data);
        this.total_items = header.total_items;
        console.log("Items to receive: "+this.total_items);
        this.act_msg = header.msg;
        this.loading = true;
      }, false);
      es.addEventListener('item', event => {
        if (this.loading) {
          let eventdata = event.data;
          let data = JSON.parse(eventdata);
          this.items.push(data);
          console.log("Item received: "+data.text);
          if (this.items.length == this.total_items) {
            console.log("All items successfully received.")
            this.loading = false;
            es.close();
          }
        }
      }, false);
      es.addEventListener('close', function() {
        this.loading = false;
        console.log('Closing SSE.');
        es.close();
      }, false);
      es.onopen = function() {
        console.log('Connection is opened.');
      }
      es.onerror = function() {
        console.log('Connection error! Trying to reconnect...');
      }
    },
  }
}
</script>

<!-- Add "scoped" attribute to limit CSS to this component only -->
<style scoped>
h3 {
  margin: 40px 0 0;
}
ul {
  list-style-type: none;
  padding: 0;
}
li {
  display: inline-block;
  margin: 0 10px;
}
a {
  color: #42b983;
}
</style>

```

The both the server...

```sh
$ python3 server.py
```

... and the client are started.

```sh
$ yarn serve
```

{{< figure src="screenshot.png" title="Links loggt die Pythonanwendung die Sendevorgänge, rechts sieht man die eintrudelnden Nachrichten." >}}

