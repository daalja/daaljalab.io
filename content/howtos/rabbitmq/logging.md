---
title: Logging
linktitle: Logging
toc: true
type: docs
date: "2020-04-03T00:00:00+01:00"
lastmod: "2020-04-03T00:00:00+01:00"
draft: false
tags: ["Python", "RabbitMQ"]
menu:
  fedora:
    parent: Übersicht
    weight: 1

# Prev/next pager order (if `docs_section_pager` enabled in `params.toml`)
weight: 1
---

[python-logging-rabbitmq](https://github.com/albertomr86/python-logging-rabbitmq)(MIT-Lizenz) ermöglicht die Logs über RabbitMQ zu pushen.

```sh
$ pip3 install python_logging_rabbitmq --user
```

Ein Beispielclient
```python
from timeit import default_timer as timer
import logging
from python_logging_rabbitmq import RabbitMQHandler

logger = logging.getLogger('myapp')
logger.setLevel(logging.DEBUG)

rabbit = RabbitMQHandler(host='localhost')
logger.addHandler(rabbit)

def test(n):
    t0 = timer()

    for i in range(1, n, 1):
        logger.debug(f'test debug {i}')

    logger.info(f'Time for {n} logs was {timer()-t0} seconds.')

test(100)
test(1000)
test(10000)
```

Ein Beispielconsumer (hier als Erweiterung eines offiziellen Beispiels von RabbitMQ).
Alle Meldungen vom Client werden über das log-Exchange behandelt. Für das topic_logs
wird die `emit_log_topic.py` aus den Beispielen verwendet.


```python
#!/usr/bin/env python
import pika
import sys

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.exchange_declare(exchange='topic_logs', exchange_type='topic')
channel.exchange_declare(exchange='log', exchange_type='topic')

result = channel.queue_declare('', exclusive=True)
queue_name = result.method.queue

binding_keys = sys.argv[1:]
if not binding_keys:
    sys.stderr.write("Usage: %s [binding_key]...\n" % sys.argv[0])
    sys.exit(1)

for binding_key in binding_keys:
    channel.queue_bind(
        exchange='topic_logs', queue=queue_name, routing_key=binding_key)
    channel.queue_bind(
        exchange='log', queue=queue_name, routing_key=binding_key)

print(' [*] Waiting for logs. To exit press CTRL+C')


def callback(ch, method, properties, body):
    print(" [x] %r:%r" % (method.routing_key, body))


channel.basic_consume(
    queue=queue_name, on_message_callback=callback, auto_ack=True)

channel.start_consuming()
```

Parallel gestartet für Info und Debug-Nachrichten:
```sh
$ python receive_logs_topic.py "#.INFO"
~
$ python receive_logs_topic.py "#.DEBUG"
```