---
title: Selectors
linktitle: Selectors
toc: true
type: docs
date: "2020-03-19T00:00:00+01:00"
lastmod: "2020-03-19T00:00:00+01:00"
draft: false
menu:
  python:
    parent: Übersicht
    weight: 1

# Prev/next pager order (if `docs_section_pager` enabled in `params.toml`)
weight: 1
---

https://pymotw.com/3/selectors/

https://docs.python.org/3.6/library/selectors.html