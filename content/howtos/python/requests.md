---
title: Requests
linktitle: Requests
toc: true
type: docs
date: "2020-03-23T00:00:00+01:00"
lastmod: "2020-03-23T00:00:00+01:00"
draft: false
menu:
  python:
    parent: Übersicht
    weight: 22

# Prev/next pager order (if `docs_section_pager` enabled in `params.toml`)
weight: 22
---


# Troubleshooting

## UTF-8 Soft Hyphen (\xc2\xad)

Soft Hyphens (https://www.utf8-chartable.de/unicode-utf8-table.pl?start=128&number=128&utf8=string-literal) werden in harte Leerzeichen im Text wiedergegeben.

SOFT HYPHEN