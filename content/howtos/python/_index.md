---
# Course title, summary, and position.
linktitle: Python
summary: Python Recherchen
weight: 20

# Page metadata.
title: Python
date: "2020-03-19T00:00:00Z"
lastmod: "2020-03-19T00:00:00Z"
draft: true  # Is this a draft? true/false
toc: true  # Show table of contents? true/false
type: docs  # Do not modify.

# Add menu entry to sidebar.
# - name: Declare this menu item as a parent with ID `name`.
# - weight: Position of link in menu.
menu:
  python:
    name: Übersicht
    weight: 20
---

