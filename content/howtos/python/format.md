---
title: Stringformat
linktitle: Stringformat
toc: true
type: docs
date: "2020-03-20T00:00:00+01:00"
lastmod: "2020-03-20T00:00:00+01:00"
draft: false
menu:
  python:
    parent: Übersicht
    weight: 20

# Prev/next pager order (if `docs_section_pager` enabled in `params.toml`)
weight: 20
---

> Betrifft: Python >= 3.6

Anstelle von der String-Format-Funktion kann man nun direkt auf ein Literal zurückgreifen.

```python
>>> today_degree=30
>>> yesterday_degree=28
>>> name= 'Welt'
>>> 'Hallo {name}'.format(name=name)
# bzw. 
>>> 'Hallo {}'.format(name)
# aber jetzt auch so:
>>> f'Hallo {name}'
# Und das auch komplexer:
>>> f'''Hallo {name.upper()}!.
        Heute sind es {today_degree-yesterday_degree}°C Unterschied zu gestern'''
```

https://cito.github.io/blog/f-strings/
