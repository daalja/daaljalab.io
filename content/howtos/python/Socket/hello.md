---
title: Hello Socket
linktitle: Hello Socket
toc: true
type: docs
date: "2020-04-12T00:00:00+01:00"
lastmod: "2020-04-12T00:00:00+01:00"
draft: false
menu:
  python:
    parent: Übersicht
    weight: 1

# Prev/next pager order (if `docs_section_pager` enabled in `params.toml`)
weight: 1
---

# Hintergrund

Pythons Socket Modul bietet eine Schnittstelle auf die Berkeley Sockets API mit den primären Funktionen:

| Server    __ | Client       |
| ------------ | ------       |
| 1. socket()  | 1. socket()  |
| 2. bind()    | -            |
| 3. listen()  | -            |
| 4. accept()  | 2. connect() |
| 5.1. send()  | 3.1. send()  |
| 5.2. recv()  | 3.2. recv()  |
| 6.1. recv()  | 4. close()   |
| 6.2. close() | -            |
| ------------ | ------------ |
| connect_ex() |              |


Häufig nutzt man TCP als Socketprotokoll.

Ein einfaches Anwendungsszenario ist ein Echo- oder Heartbeatdienst, d.h. der Server schickt dem Client die übermittelte Nachricht unverändert zurück bzw. antwortet auf ein *PING* mit einem *PONG*.

Der Server wartet auf Verbindungsanforderungen, zeigt Informationen zum Anrufer auf der Konsole und sendet diesem die übermittelte Nachricht postwendend zurück. Im Beispiel hört der Dienst auf dem Loopback-Interface (127.0.0.1). Alternativ kann die IP eines *entfernten* Rechners oder sein Hostname angegeben werden. Gibt man einen leeren String an, hört der Dienst auf alle Anfragen im TCP-Stack, also sowohl dem Loopback-Interface, als auch allen fernen Rechnern. Wird ein leerer Datenstrom übermittelt, beendet der Server seinen Dienst.

``` python
import time
import socket

HOST = '127.0.0.1'
PORT = 5000

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
  sock.bind((HOST, PORT))
  sock.listen()
  with sock:
    while True:
      connection, address = sock.accept() # Wait for a client (blocking)
      try:
        data = connection.recv(1024)
        if data == b'EXIT':
          print('Closing connection.')
          connection.sendall(b'Server shutdown.')
          break
        elif data == b'PING':
          print(f'Heartbeat {address}')
          connection.sendall(b'PONG')
        else:
          print(f'Echoing message to {address}')
          connection.sendall(data)
      except KeyboardInterrupt:
        print('Closing connection.')
        break
      time.sleep(0.1)

```

Wird eine Verbindung aufgebaut, so ist sie blockierend. Das heißt, dass der Server erst wieder eine neue Anfrage bearbeiten kann, nachdem er die aktuelle abgearbeitet hat.

{{% alert note %}}
Dieses einfache Beispiel behandelt nicht die typischen Exceptions einer Netzwerkdienstes. Abgebrochene Verbindungen, ...
{{% /alert %}}

Der Echo-Client kann wie folgt aussehen:

``` python
import socket
import argparse

HOST = '127.0.0.1'
PORT = 5000

parser = argparse.ArgumentParser()
parser.add_argument('-e', '--exit', action='store_true',
    help="exit call")

args = parser.parse_args()

if args.exit:
    response = b'EXIT'
else:
    response = b'PING'

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
  sock.connect((HOST, PORT))
  sock.sendall(response)
  data = sock.recv(1024)

print(f'Received "{data.decode()}".')
```

# Socketserver

Für Python existiert das Modul Socketserver, welches viele der Low-Level-Operationen obsolete macht.

``` python
import socketserver
from time import sleep
from timeit import default_timer as timer

class HeartbeatHandler(socketserver.BaseRequestHandler):
    """
    Request handler class.

    This will be instantiated once per connection. The communication
    is defined in the handle() method.
    """

    def handle(self):
        self.data = self.request.recv(1024).strip()
        if self.data == b'EXIT':
          print('Server shutdown.')
          self.request.sendall(b'Server shutdown.')

          # self.server.shutdown() would deadlock in a single threaded app!
          self.server._BaseServer__shutdown_request = True
        elif self.data == b'PING':
          print(f'Heartbeat {self.client_address}')
          self.request.sendall(b'PONG')
        else:
          print(f'Echoing message to {self.client_address}')
          self.request.sendall(self.data)

if __name__ == "__main__":
    HOST, PORT = "127.0.0.1", 5000
    start = timer()

    with socketserver.TCPServer((HOST, PORT), HeartbeatHandler) as server:
        # Activate the server; this will keep running until you
        # interrupt the program with Ctrl-C
        try:
          server.serve_forever()
        except (KeyboardInterrupt, SystemExit):
          pass
    end = timer()
    print(f"Took {end-start:.2f} seconds")
```

# Test

Die beiden Server werden über ein Bash-Skript befeuert:

``` sh
#!/bin/bash

PROCESS='client.py'
COUNTER=0
COUNT=100

while [ $COUNTER -lt $COUNT ];
do
    python3 ./client.py &
    let COUNTER=COUNTER+1
    echo $COUNTER
done
sleep 3 # Give server.py some time to work
python3 ./client.py --exit &
echo "Fin"
```

# Verbindungsfehler

## Client

### Errno 104 Connection reset by peer

Wird der Server bspw. vorzeitig beendet, kommt es zu diesem Fehler bei einer Datenübertragung (`send` und `recv`).


# Quellen

1. https://realpython.com/python-sockets/
2. https://developer.ibm.com/technologies/python/tutorials/l-pysocks/