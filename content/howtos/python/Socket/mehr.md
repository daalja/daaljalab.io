---
title: Mehr...
linktitle: Mehr...
toc: true
type: docs
date: "2020-04-12T00:00:00+01:00"
lastmod: "2020-04-12T00:00:00+01:00"
draft: false
menu:
  python:
    parent: Übersicht
    weight: 1

# Prev/next pager order (if `docs_section_pager` enabled in `params.toml`)
weight: 100
---

Um den Socket nicht blockierend zu nutzen, gibt es verschiedene Möglichkeiten:

* Threads
* Asynchrone, betriebssystemunterstützte Sockets ([How to Work with TCP Sockets in Python (with Select Example)](https://steelkiwi.com/blog/working-tcp-sockets/) [1])

Threads zu erzeugen und zwischen diesen zu wechseln ist eine umfangreiche Operation. Performanter ist die asynchrone Variante, bei der das Betriebssystem den Socketstatus überwacht und das Programm informiert, sobald etwas zu bearbeiten vorliegt.

Folgende Schnittstellen gibt es zu diesem Zweck:

* poll, epoll (Linux)
* select (plattformübergreifend)

``` python
import select, socket, sys, Queue
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.setblocking(0)
server.bind(('127.0.0.1', 5000))
server.listen(5)
inputs = [server]
outputs = []
message_queues = {}

while inputs:
    # Das Betriebssystem soll die Sockets überwachen auf Schreib- und Leseoperationen und
    # aufgetretene Fehler. Die Liste für die Inputs wird auch zur Auswertung der Fehler
    # genutzt.
    readable, writable, exceptional = select.select(
        inputs, outputs, inputs)
    for s in readable:
        if s is server:
            # Sobald ein Socket von außen angefragt wird, wird
            # die Verbindung akzeptiert und der Liste inputs
            # hinzugefügt. Desweiteren wird eine Queue für die
            # eingehenden Nachrichten erstellt
            connection, client_address = s.accept()
            connection.setblocking(0)
            inputs.append(connection)
            message_queues[connection] = Queue.Queue()
        else:
            # Empfange Nachrichten eines Sockets in dessen Queue
            data = s.recv(1024)
            if data:
                message_queues[s].put(data)
                if s not in outputs:
                    outputs.append(s)
            else:
                if s in outputs:
                    outputs.remove(s)
                inputs.remove(s)
                s.close()
                del message_queues[s]

    for s in writable:
        try:
            next_msg = message_queues[s].get_nowait()
        except Queue.Empty:
            outputs.remove(s)
        else:
            s.send(next_msg)

    for s in exceptional:
        inputs.remove(s)
        if s in outputs:
            outputs.remove(s)
        s.close()
        del message_queues[s]
```


# Quellen

1. https://steelkiwi.com/blog/working-tcp-sockets/