---
title: Vue.js und Flask
linktitle: Vue.js und Flask
toc: true
type: docs
date: "2020-03-11T00:00:00+01:00"
lastmod: "2020-03-13T00:00:00+01:00"
draft: false
menu:
  vue-flask-rki:
    parent: Übersicht
    weight: 4

# Prev/next pager order (if `docs_section_pager` enabled in `params.toml`)
weight: 4
---

## Vue+Axios und Flask-RESTful API

Um das Javascript-Framework Vue.js mit einem Python-Backend wie Flask zu verbinden, benötigt man eine geeignete Schnittstelle. Nachdem ich im letzten Kapitel eine RESTful API mit Flask generiert habe, kann ich diese nun nutzen, um Vue mit Daten zu füttern. Wie eine solche Implementierung weiter erfolgen kann ist ausführlich in Miguel Grinbergs Blog beschrieben ([Designing a RESTful API with Python and Flask](https://blog.miguelgrinberg.com/post/designing-a-restful-api-with-python-and-flask) ).

Hier soll nur kurz die Verbindung zu Vue.js dargestellt werden.

Der Einfachheit halber wird das Vue-Frontend zusammen mit dem Backend-Code in einem Ordner gehalten.

```sh
$ ls
app.py
$ vue create frontend
Features: Babel, Router, Linter
$ cd frontend
$ yarn add axios vue-axios vuex 
$ yarn serve
```

Unter src wird die Datei store.js für den VUEX-Store erstellt.

```js
import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(Vuex)
Vue.use(VueAxios, axios)

export default new Vuex.Store({
  state: {
     casenumbers: []
  },
  actions: {
    loadCasenumbers ({ commit }) {
        axios
          .get('http://localhost:5000/api/v1.0/fallzahlen')
          .then(r => r.data)
          .then(casenumbers => {
          console.log(casenumbers)
          commit('SET_CASENUMBERS', casenumbers)
          })
      }
  },
  mutations: {
    SET_CASENUMBERS (state, casenumbers) {
        state.casenumbers = casenumbers
    }
  }
})
```

Der Store wird zentral registriert.

```js
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
```

Zur Darstellung wird eine Covid19-Komponente erstellt.

```js
<template>
  <div class="container">
    <table class="table table-stribed">
      <thead>
        <tr>
          <th>Bundesland</th>
          <th>Aktuelle Fälle</th>
          <th>Aktuelle Todesfälle</th>
        </tr>
      </thead>
      <tbody>
        <tr v-for="casenumber in casenumbers" :key="casenumber.id">
          <td>{{ casenumber.bundesland }}</td>
          <td>{{ casenumber.faelle }}</td>
          <td>{{ casenumber.todesfaelle }}</td>
        </tr>
      </tbody>
    </table>
  </div>
</template>

<script>
import { mapState } from 'vuex'
export default {
  name: 'HelloWorld',
  mounted () {
    this.$store.dispatch('loadCasenumbers')
  },
  computed: mapState([
    'casenumbers'
  ])
}
</script>
```

Und diese Komponente wird in der Home.vue eingebettet.

```js
<template>
  <div class="home">
    <Covid19/>
  </div>
</template>

<script>
// @ is an alias to /src
import Covid19 from '@/components/Covid19.vue'

export default {
  name: 'Home',
  components: {
    Covid19
  }
}
</script>
```

Für den Fall, dass keine Ergebnisse angezeigt werden, sieht man sich die Konsole in den Entwicklungstools des Browsers an.

> Ein möglicher Fehler ist die Blockade von Cross-Origin Requests (CORS). Siehe hierzu das Kapitel [Flask](/howtos/vue-flask/flask/#cross-origin-resource-sharing).

Gestartet werden müssen das Flask-Backend und das Vue-Frontend beispielsweise über zwei Terminals:

```sh
$ flask run
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: off
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
```

```sh
$ cd frontend
$ yarn serve
yarn run v1.22.4
$ vue-cli-service serve
 INFO  Starting development server...
98% after emitting CopyPlugin

 DONE  Compiled successfully in 3028ms                                                       12:48:47 PM


  App running at:
  - Local:   http://localhost:8080/ 
  - Network: http://192.168.178.116:8080/

  Note that the development build is not optimized.
  To create a production build, run yarn build.
```

Das Ergebnis präsentiert sich dann so:


{{< figure src="browser.png" title="Vue.js Welcome App" lightbox="true" >}}

## Eyecandy

Schön sieht das Ergebnis ja nun nicht aus. Es gibt Erweiterungen für Vue, die einem das Arbeiten mit Bootstrap und Co etwas erleichtern. Beliebt ist zum Beispiel [Vuetify](https://vuetifyjs.com/). Gleichzeitig erhöht das die Abhängigkeiten derart, dass man es in der Dauer und Größe der Paketierung merkt. Für unseren Zweck reicht eine einfache Integration von [Bootstrap](https://bootstrap-vue.js.org) völlig aus.

```sh
$ vue add bootstrap-vue
```

Mit diesem Modul kann nun die Tabelle [schöner gestaltet](https://bootstrap-vue.js.org/docs/components/table) werden.

```js
<template>
  <div class="container">
    <b-table striped hover :items="casenumbers" :fields="fields"></b-table>
  </div>
</template>

<script>
import { mapState } from 'vuex'
export default {
  name: 'Covid-19',
  mounted () {
    this.$store.dispatch('loadCasenumbers')
  },
  computed: mapState([
    'casenumbers'
  ]),
  data () {
    return {
      fields: [
        {
          key: 'bundesland',
          sortable: true
        },
        {
          key: 'faelle',
          label: 'Zahl bestätigter Fälle',
          sortable: true
        },
        {
          key: 'todesfaelle',
          label: 'Zahl bestätigter Todesfälle',
          sortable: true,
        }
      ]
    }
  }
}
</script>
```

{{< figure src="bootstrap.png" title="Dank Bootstrap schaut die Tabelle schonmal ansprechender aus" lightbox="true" >}}

Was könnte man nun weiter tun?

* Gesamtspalte extrahieren und als Footer darstellen. Derzeit wird sie mit den Bundesländern sortiert
* Würde der Dienst dauerhaft laufen, könnte ich einen Verlauf speichern und graphisch darstellen oder anderweitig auswerten.

## Reload

Soweit so gut. Doch erfolgt nur auf ein Frontend-Event durch den Anwender die Abfrage der Daten vom Backend. Änderungen im Backend werden nicht aktiv an das Frontend übermittelt. Behelfen kann man sich mit einem Javascript-Timer. Ein einfacher (und ziemlich dummer) Algorithmus holt sich also alle paar Sekunden die Daten vom Server. Da diese sich nur jeden Tag einmal ändern, werden somit sehr viele Abrufe unnötig ausgeführt. In einem lokalen System ist dieser Overhead nicht schön aber zumindest vernachlässigbar klein.

Dazu wird in Covid19.vue das Skript angepasst:

```js
<script>
function partial(func /*, 0..n args */) {
  // Source: https://stackoverflow.com/a/321527
  var args = Array.prototype.slice.call(arguments, 1);
  return function() {
    var allArguments = args.concat(Array.prototype.slice.call(arguments));
    return func.apply(this, allArguments);
  };
}

import { mapState } from 'vuex'
export default {
  name: 'Covid-19',
  mounted () {    
    let floadCasenumbers = partial(this.$store.dispatch, 'loadCasenumbers');
    floadCasenumbers();
    this.timer = setInterval(floadCasenumbers, 10000);
  },
  computed: mapState([
    'casenumbers'
  ]),
  beforeDestroy() {
    // Der in mounted erzeugte Timer muss aktiv beim Seitenwechsel gelöscht werden!
    clearInterval(this.timer)
  },
  data () {
    return {
      fields: [
        {
          key: 'bundesland',
          sortable: true
        },
        {
          key: 'faelle',
          label: 'Zahl bestätigter Fälle',
          sortable: true
        },
        {
          key: 'todesfaelle',
          label: 'Zahl bestätigter Todesfälle',
          sortable: true,
        }
      ]
    }
  },
}
</script>
```
Dahinter steckt ein Javascript-Timer der via setInterval aktiviert und per ClearInterval wieder gelöscht wird. Da setInterval eine Referenz übergeben haben möchte, wird diese über ein Partial erzeugt (siehe zum Beispiel [Ben Almans Erläuterungen zu diesem Thema](http://benalman.com/news/2012/09/partial-application-in-javascript/)).


{{< figure src="update.png" title="Regelmäßige Updates getriggert durch den Client" lightbox="true" >}}

## Socket.IO

Socket.IO kombiniert Websockets und Polling und lässt sich direkt in Python und Vue nutzen.

## Websocket

[Websockets](https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API/Writing_WebSocket_client_applications) lassen sich natürlich auch direkt nutzen.

Beispielsweise könnte eine Komponente so aussehen:

```js
<script>
export default {
  ...,
  created() {
    this.connectToWebSocket()
  }
  ...
  methods: {
    startSession () {
      // Für den Fall, dass die Anwendung ein Session Management braucht
      this.connectToWebSocket()
    },
    connectToWebSocket () {
      const websocket = new WebSocket(`ws://localhost:8081/${this.$route.params.uri}`)
      websocket.onopen = this.onOpen
      websocket.onclose = this.onClose
      websocket.onmessage = this.onMessage
      websocket.onerror = this.onError
    },
    onClose (event) {
      console.log('Connection closed.', event.data)
      // Try and Reconnect after five seconds
      setTimeout(this.connectToWebSocket, 5000)
    },
  }
}
</script>
```
Idee: [Github-Projekt Chatire](https://github.com/danidee10/Chatire/blob/8c578e9c4fb1c31e35637813dc3503816a9f975a/chatire-frontend/src/components/Chat.vue)

> Möglicherweise werden die Funktionen nicht mehr unter methods geführt (war zumindest so bei der beforeDestroy-Funktion im Reload-Kapitel).

Was das Beispielprojekt Chatire nicht macht ist die Senden-Funktion der Websockets zu verwenden. Dafür werden normale GET-Aufrufe gegen RabbitMQ ausgeführt.

Die Mozilla-Beschreibung bzw. das [Beispielprojekt auf Github](https://github.com/mdn/samples-server/blob/master/s/websocket-chat/chatclient.js) erläutert aber recht gut, wie einfach das ist:

```js
// Send text to all users through the server
function sendText() {
  // Construct a msg object containing the data the server needs to process the message from the chat client.
  var msg = {
    type: "message",
    text: document.getElementById("text").value,
    id:   clientID,
    date: Date.now()
  };

  // Send the msg object as a JSON-formatted string.
  websocket.send(JSON.stringify(msg));
} 
```