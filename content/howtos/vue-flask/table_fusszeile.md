---
title: Erweiterung - Tabellenfußzeile
linktitle: Erweiterung - Tabellenfußzeile
toc: true
type: docs
date: "2020-03-17T00:00:00+01:00"
draft: false
menu:
  vue-flask-rki:
    parent: Übersicht
    weight: 6

# Prev/next pager order (if `docs_section_pager` enabled in `params.toml`)
weight: 6
---

Damit die Gesamtsumme nicht mit den Bundesländern mit sortiert wird, extrahiere ich sie aus den JSON-Daten und zeige sie in einer Fußzeile an. Dazu verändere ich nicht die Datenquelle (das Pythonskript), sondern erledige die Verarbeitung rein auf Frontendseite.

Der Vuex-Store extrahiert zunächst die Zeile mit der Summe.

```js
import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(Vuex)
Vue.use(VueAxios, axios)

export default new Vuex.Store({
  state: {
     casenumbers: [],
     sum: null
  },
  actions: {
    loadCasenumbers ({ commit }) {
        axios
          .get('http://localhost:5000/api/v1.0/fallzahlen')
          .then(r => r.data)
          .then(casenumbers => {
            var length = casenumbers.length
            var sum = {
              faelle: 0,
              todesfaelle: 0
            }
            var lastitem = casenumbers[length-1]
            if (lastitem.bundesland == 'Gesamt')
              sum.faelle = lastitem.faelle
              sum.todesfaelle = lastitem.todesfaelle
              casenumbers.splice(length-1,1)
            commit('SET_CASENUMBERS', {casenumbers, sum})
          })
      }
  },
  mutations: {
    SET_CASENUMBERS (state, payload) {
        state.casenumbers = payload.casenumbers
        state.sum = payload.sum
    }
  }
})
```

Die Covid-Komponente zeigt diese Daten dann in einer Fußzeile an. Hierzu wird ein Computed-Element ergänzt und die Tabelle um die Fußzeile mit Zugriff auf dieses Element erweitert.

```js
<template>
  <div class="container">
    <b-table striped hover :items="casenumbers" :fields="fields">
      <template slot="bottom-row" >
        <td>Gesamt</td>
        <td>{{ sum.faelle }}</td>
        <td>{{ sum.todesfaelle }}</td>
      </template>
    </b-table>
  </div>
</template>

<script>
function partial(func /*, 0..n args */) {
  // Source: https://stackoverflow.com/a/321527
  var args = Array.prototype.slice.call(arguments, 1);
  return function() {
    var allArguments = args.concat(Array.prototype.slice.call(arguments));
    return func.apply(this, allArguments);
  };
}

import { mapState } from 'vuex'
export default {
  name: 'Covid-19',
  mounted () {    
    let floadCasenumbers = partial(this.$store.dispatch, 'loadCasenumbers');
    floadCasenumbers();
    this.timer = setInterval(floadCasenumbers, 10000);
  },
  computed: mapState([
    'casenumbers',
    'sum'
  ]),
  beforeDestroy() {
    // Der in mounted erzeugte Timer muss aktiv beim Seitenwechsel gelöscht werden!
    clearInterval(this.timer)
  },
  data () {
    return {
      fields: [
        {
          key: 'bundesland',
          sortable: true
        },
        {
          key: 'faelle',
          label: 'Zahl bestätigter Fälle',
          sortable: true
        },
        {
          key: 'todesfaelle',
          label: 'Zahl bestätigter Todesfälle',
          sortable: true,
        }
      ]
    }
  },
}
</script>

```
