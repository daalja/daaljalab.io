---
title: Datenquelle
linktitle: Datenquelle
toc: true
type: docs
date: "2020-03-11T00:00:00+01:00"
lastmod: "2020-03-12T00:00:00+01:00"
draft: false
menu:
  vue-flask-rki:
    parent: Übersicht
    weight: 3

# Prev/next pager order (if `docs_section_pager` enabled in `params.toml`)
weight: 3
---

Von wem und wie bekommt man nun aktuelle Zahlen zu Corona bzw. genauer COVID-19?

In und für Deutschland hat das [Robert Koch-Institut](https://www.rki.de/DE/Home/homepage_node.html) die [aktuellsten Zahlen](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Fallzahlen.html). 

Europaweit werden die tagesaktuellen Fallzahlen durch das [ECDC](https://www.ecdc.europa.eu) gesammelt und als [Exceldatei](https://www.ecdc.europa.eu/en/publications-data/download-todays-data-geographic-distribution-covid-19-cases-worldwide) zur Verfügung gestellt.

Tagesaktuelle [Fallzahlen weltweit](https://experience.arcgis.com/experience/685d0ace521648f8a5beeeee1b9125cd) stellt die [WHO](https://www.who.int/). Datentechnisch verantwortlich für das [GIS-Dashboard](https://experience.arcgis.com/experience/685d0ace521648f8a5beeeee1b9125cd) der WHO ist das [CSSE](https://systems.jhu.edu/research/public-health/ncov/) der [JHU](https://jhu.edu). Die JHU stellt die Daten in einem eigenen [Dashboard](https://www.arcgis.com/apps/opsdashboard/index.html#/bda7594740fd40299423467b48e9ecf6) dar und bietet die Datenbank über [Github](https://github.com/) [öffentlich und frei verfügbar](https://github.com/CSSEGISandData/COVID-19) an.

### Web-/Screenscraping

Ein unter Python gern genutztes Konzept ist das Webscraping, bei dem die Daten direkt aus Webseiten extrahiert werden. Beim Datensatz des RKI scheint es auf den ersten Blick keine andere Möglichkeit zu geben. Da Webscraping jedoch auch mit relevanten Nachteilen einhergeht, bietet sich die Suche nach sinnvolleren Alternativen immer an. Die Nachteile des Webscraping wären:

* Je nach Nutzungsbedingungen kann das automatisierte Parsen rechtlich problematisch sein. So können sowohl Urheberrechtsansprüche bestehen, was bedeutet, dass auch eine Datenbank in der dargestellten Form geschützt sein kann. Weitere Informationen findet man zum Beispiel bei den RAs [Wilde Beuger Solmecke (letzter Abruf 13.03.2020)](https://www.wbs-law.de/urheberrecht/ist-screen-scraping-legal-15081/).
* Unabhängig von rechtlichen Komplikationen ist es dem Serverbetreiber jederzeit möglich auffällige Zugriffsmuster zu erkennen und die Abfragen aktiv zu blockieren.
* Bei einem Fehler im Skript könnte der Abruf möglicherweise einen Server lahmlegen wodurch u.U. rechtliche Probleme entstehen könnten.
* Der Betreiber kann jederzeit das Interface verändern, womit das Skript erst wieder korrigiert werden muss, bevor was sinnvolles rauskommt.

Unter Python könnte ein kurzes Skript so aussehen:

```python
import requests
import json
import re
from bs4 import BeautifulSoup

def format_as_expected(source):
    header = source.findAll('th')
    return (len(header) > 0 and
        header[0].text.strip() == 'Bundesland' and
        'Zahl be­stä­tig­ter Fälle' in header[1].text.strip())

def parse_table(source):
    body = source.find('tbody')
    dataset = []
    for table_row in body.find_all('tr'):
        cells = table_row.findAll('td')
        if len(cells) > 0:
            matched = re.match(r'(\d*)(?:\s\((\d*)\))?', cells[1].text.strip())
            faelle = matched.group(1)
            if len(matched.groups()) > 1:
                todesfaelle = matched.group(2)
            else: 
                todesfaelle = 0
            data = {
                'bundesland': cells[0].text.strip(),
                'faelle': faelle,
                'todesfaelle': todesfaelle
            }
            dataset.append(data)
    ''' Gib entweder die Daten direkt zurück
    return dataset
    oder lass sie durch den json-Parser laufen
    return json.dumps(dataset)
    Bei letzterem muss jedoch bereits auf Umlaute geachtet werden.
    '''
    return json.dumps(dataset, ensure_ascii=False)

if __name__ == "__main__":
    # url = 'https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Fallzahlen.html'
    url = 'https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Fallzahlen.html'
    response = requests.get(url)
    soup = BeautifulSoup(response.text, "html.parser")
    table = soup.find("table") # Suche die erste Tabelle
    if format_as_expected(table):
        data = parse_table(table)
        print(data)
```
> Sollte obiges Skript unabhängig von tatsächlichen Inhaltsänderungen nicht funktionieren, kann das z.B. an den Zeichen liegen. Eine print-Ausgabe des Headers hatte bei mir z.B. gezeigt, dass "Zahl be­stä­tig­ter Fälle" abwich von meiner Variante. Also entweder den Code 
robuster machen oder wenn's schnell und einfach gehen soll, den Text testweise mal aus der Webseite per Copy'n'Paste übernehmen...

Wie schnell das mit den Änderungen der Datenquelle gehen kann, hat sich beim Schreiben des Skriptes gezeigt. Plötzlich hat sich die Internetseite geändert, wie folgende Screenshots zeigen.

{{< figure src="before.png" title="Die Tabelle am 11.03.2020" lightbox="true" >}}

{{< figure src="after.png" title="Die Tabelle vom 12.03.2020" lightbox="true" >}}


### JHU-Datensatz auf Github

Nachdem das RKI die Daten täglich an die WHO bzw. die JHU meldet und diese die Daten auch täglich kostenlos zur Verfügung stellt ist die Github-Quelle vorzuziehen.

> Das gilt aber nur sofern einem die Gesamtdaten von Deutschland ausreichen. Die einzelnen Bundesländer sind hier nicht aufgedröselt.

Natürlich hat sich da bereits jemand gefunden, der ein Pythonskript entwickelt hat ([ExpDev07/coronavirus-tracker-api](https://github.com/ExpDev07/coronavirus-tracker-api)). Das Skript könnte somit als Basis für eine eigene Entwicklung dienen oder man kann damit lokal eine Webservice ausführen und per RESTful API abfragen. Aber warum so kompliziert, wenn der Entwickler die API bereits über herokuapp öffentlich zur Verfügung stellt?

### RESTful API

Im Sinne eines Microservices- oder zumindest Webservices-Konzeptes werden Daten dynamisch und kontextabhängig abgefragt. Über ein definiertes Interface frage ich die Daten ab, die mich gerade interessieren. Der Server filtert die Informationen und liefert sie mir entsprechend zurück.

Im Falle des durch [ExpDev07/coronavirus-tracker-api](https://github.com/ExpDev07/coronavirus-tracker-api) auf Heroku angebotenen Dienstes sieht das wie folgt aus:

$URL: https://coronavirus-tracker-api.herokuapp.com

Abfrage aller Daten:
```
GET $URL/all
```
Abfrage der bestätigten Erkrankungen:
```
GET $URL/confirmed
```
Abfrage der Anzahl der verstorbenen Patienten:
```
GET $URL/deaths
```
Abfrage der genesenen Patienten:
```
GET $URL/recovered
```

Um die per Webscraping erfassten Daten über eine eigene API anzubieten, schreibe ich mir ein kleines Flask-Skript. Dabei läuft im Hauptthread eine Flaskapplikation die das RESTful API-Interface anbietet. Im Hintergrundthread wird jeden Tag ein Datenabruf vorgenommen. Der Austausch der Daten zwischen den Threads erfolgt über eine Queue.

{{< figure src="fallzahlen.png" title="Die Tabelle vom 12.03.2020" lightbox="true" >}}

```python
import requests
import json
import time
from threading import Thread
import queue
import re
from bs4 import BeautifulSoup
from flask import Flask, jsonify

thread = None
fallzahlen = queue.Queue(1)

def background_update(fallzahlen):
    while True:
        url = 'https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Fallzahlen.html'
        response = requests.get(url)
        soup = BeautifulSoup(response.text, "html.parser")
        table = soup.find("table") # Suche die erste Tabelle
        if format_as_expected(table):
            fallzahlen.empty() # Überschreibe alte Daten
            print("Overriding cache...")
            fallzahlen.put(parse_table(table))
        else:
            print("Fehler: Tabellenformat hat sich vermutlich verändert...")
            exit()
        time.sleep(60*60*24)   

def format_as_expected(source):
    header = source.findAll('th')
    return (len(header) > 0 and
        header[0].text.strip() == 'Bundesland' and
        'Zahl be­stä­tig­ter Fälle' in header[1].text.strip())

def parse_table(source):
    body = source.find('tbody')
    dataset = []
    for table_row in body.find_all('tr'):
        cells = table_row.findAll('td')
        if len(cells) > 0:
            matched = re.match(r'(\d*)(?:\s\((\d*)\))?', cells[1].text.strip())
            faelle = matched.group(1)
            if len(matched.groups()) > 1:
                todesfaelle = matched.group(2)
            else: 
                todesfaelle = 0
            data = {
                'bundesland': cells[0].text.strip(),
                'faelle': faelle,
                'todesfaelle': todesfaelle
            }
            dataset.append(data)
    ''' Gib entweder die Daten direkt zurück
    return dataset
    oder lass sie durch den json-Parser laufen
    return json.dumps(dataset)
    Bei letzterem muss jedoch bereits auf Umlaute geachtet werden.
    '''
    return json.dumps(dataset, ensure_ascii=False)

app = Flask(__name__)

@app.route('/api/v1.0/fallzahlen', methods=['GET'])
def get_states():
    global thread
    if thread is None:
        print("Backgroundthread started...")
        thread = Thread(target=background_update, args=(fallzahlen, ))
        thread.start()
    if fallzahlen.not_empty:
        ''' Damit immer Werte vorhanden sind, dürfen wir
        die bei einem Thread-Durchlauf erhaltenen Werte
        nicht gleich verlieren.
        '''
        temp = fallzahlen.get()
        fallzahlen.put(temp)
        return jsonify(json.loads(temp))
    return 

if __name__ == '__main__':
    app.run(debug=True)
```

## Socket.io

* http://timmyreilly.azurewebsites.net/flask-socketio-and-more/
* https://github.com/timmyreilly/Demo-Flask-SocketIO/blob/master/main.py


### Weiterführende Informationen

1. https://towardsdatascience.com/how-to-web-scrape-with-python-in-4-minutes-bc49186a8460, zuletzt aufgerufen am 12.03.2020
2. https://www.digitalocean.com/community/tutorials/how-to-use-vue-js-and-axios-to-display-data-from-an-api