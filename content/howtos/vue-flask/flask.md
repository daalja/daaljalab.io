---
title: Flask
linktitle: Flask
toc: true
type: docs
date: "2020-03-11T00:00:00+01:00"
lastmod: "2020-03-13T00:00:00+01:00"
draft: false
menu:
  vue-flask-rki:
    parent: Übersicht
    weight: 1

# Prev/next pager order (if `docs_section_pager` enabled in `params.toml`)
weight: 1
---

Flask ist ein kompaktes Python-Webframework. 

## Installation

Nach der Installation aller Abhängigkeiten erstelle ich in meiner Fedora 31-Umgebung ein simples Hello-World-Programm.

```sh
$ sudo dnf update
$ sudo dnf install python3 python3-pip
$ pip3 install Flask --user
```

## Hello World

Das Beispielskript app.py

```python
from flask import Flask
app = Flask(__name__)

@app.route("/")
def hello():
    return "Hallo, Welt!"

if __name__ == "__main__":
    app.run()
```

... wird lokal ausgeführt:

```sh
$ flask run
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: off
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
127.0.0.1 - - [11/Mar/2020 17:51:06] "GET / HTTP/1.1" 200 -
```

{{< figure src="browser.png" title="Flasks Hello World in a browser" lightbox="true" >}}

## Cross-Origin Resource Sharing

An dieser Stelle sei bereits auf den Cross-Origin Resource Sharing-Mechanismus (CORS) hingewiesen, der beispielsweise bei der Abfrage von Bildern oder einer API relevant werden kann. Das Thema ist ausführlich erläutert in [Mozillas Web Docs](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS). Für Flask gibt es dazu die Erweiterung [flask_cors](https://flask-cors.readthedocs.io/en/latest/), die in ihrer einfachsten Form im Webscraper-Beispiel verwendet wird. Folgendes Skript ist vergleichbar:

```python
from flask import Flask
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

@app.route("/")
def hello():
  return "Hallo, Welt!"

if __name__ == "__main__":
    app.run()
```

Allein durch die zwei Zeilen mehr umgeht man CORS-Fehler im Browser.