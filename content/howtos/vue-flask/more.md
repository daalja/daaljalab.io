---
title: Mehr zum Thema
linktitle: Mehr zum Thema
toc: true
type: docs
date: "2020-03-13T00:00:00+01:00"
draft: false
menu:
  vue-flask-rki:
    parent: Übersicht
    weight: 5

# Prev/next pager order (if `docs_section_pager` enabled in `params.toml`)
weight: 5
---

Wer mehr zum Thema erfahren möchte, kann es hier probieren:

* https://covid19.locale.ai/\
    Vue.js-basiertes Covid19-Dashboard mit Link zum Github-Repository