---
title: Vue.js
linktitle: Vue.js
toc: true
type: docs
date: "2020-03-11T00:00:00+01:00"
draft: false
menu:
  vue-flask-rki:
    parent: Übersicht
    weight: 2

# Prev/next pager order (if `docs_section_pager` enabled in `params.toml`)
weight: 2
---

Flask ist ein kompaktes Python-Webframework. 

## Installation

Eine [von mehreren Möglichkeiten](https://vuejs.org/v2/guide/installation.html) ein [Vue.js](https://vuejs.org/)-Projekt zu starten bietet die [Vue CLI](https://cli.vuejs.org/).

Zunächst wird yarn als Paketmanager unter Fedora 31 installiert.

```sh
$ curl --silent --location https://dl.yarnpkg.com/rpm/yarn.repo | sudo tee /etc/yum.repos.d/yarn.repo
$ sudo dnf install yarn
$ yarn -v
1.22.4
```

Nun kann die Vue CLI installiert werden

```sh
$ sudo yarn global add @vue/cli
```

## Welcome Application

Dank der CLI kann nun schnell ein Rahmen für eine neue Anwendung geschaffen werden.

```sh
$ vue create welcome
$ cd welcome
$ yarn serve
```

{{< figure src="browser.png" title="Vue.js Welcome App" lightbox="true" >}}
