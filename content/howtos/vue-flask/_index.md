---
# Course title, summary, and position.
linktitle: Vue-Flask
summary: Kombiniere ein Vue-Frontend mit einem Flask-Backend (Python)
weight: 2

# Page metadata.
title: Vue-Flask
date: "2020-03-11T00:00:00Z"
lastmod: "2020-03-11T00:00:00Z"
draft: true  # Is this a draft? true/false
toc: true  # Show table of contents? true/false
type: docs  # Do not modify.

# Add menu entry to sidebar.
# - name: Declare this menu item as a parent with ID `name`.
# - weight: Position of link in menu.
menu:
  vue-flask-rki:
    name: Übersicht
    weight: 2
---

## Zielsetzung

Ziel dieses Logs ist es zu dokumentieren, wie ich ein [Vue](https://vuejs.org/)-Frontend mit einem [Flask](https://palletsprojects.com/p/flask/)-Backend koppeln kann.
